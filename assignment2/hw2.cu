#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cassert>

#define checkCudaOK(val) {\
    if(val != cudaSuccess) {\
        fprintf(stderr, "Cuda check failed at %s:%d '%s'\n", __FILE__, __LINE__, #val); \
        fprintf(stderr, "%s\n", cudaGetErrorString(val));\
        exit(EXIT_FAILURE);\
    }\
}

class CudaTimer
{
public:
    CudaTimer()
    {
        cudaEventCreate(&m_begin);
        cudaEventCreate(&m_end);
    }

    ~CudaTimer()
    {
        cudaEventDestroy(m_begin);
        cudaEventDestroy(m_end);
    }

    void begin()
    {
        cudaEventRecord(m_begin, 0);
    }

    void end()
    {
        cudaEventRecord(m_end, 0);
    }

    float get()
    {
        float duration = 0.0f;
        cudaEventSynchronize(m_begin);
        cudaEventSynchronize(m_end);
        cudaEventElapsedTime(&duration, m_begin, m_end);
        return duration;
    }

private:
    CudaTimer(CudaTimer& rhs)
    {
        // Disallow copying
    }

    cudaEvent_t m_begin;
    cudaEvent_t m_end;
};

int * alloc_and_init(size_t n)
{
    srand(0);

    int *iarr = new int[n];

    for(size_t i = 0; i < n; ++i)
    {
        iarr[i] = rand();
    }
    return iarr;
}

__global__
void gpuMaxStep(int * const d_iarr, const size_t n, const size_t offset, const size_t iterations)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    size_t i;
    int first_elem = d_iarr[idx];
    size_t copy_idx = idx + offset;
    for(i = 1; i < iterations; i++, copy_idx += offset)
    {
        int elem = d_iarr[copy_idx];
        if(elem > first_elem)
        {
            first_elem = elem;
        }
    }
    d_iarr[idx] = first_elem;
}

__global__
void gpuMaxStep16(int * const d_iarr, const size_t n, const size_t offset)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    size_t i;
    int first_elem = d_iarr[idx];
    int second_elem = d_iarr[idx + offset];
    int third_elem = d_iarr[idx + 2 * offset];
    int fourth_elem = d_iarr[idx + 3 * offset];
    for(i = 1; i < 4; i++)
    {
        int test_elem0 = d_iarr[idx + offset * 4 * i];
        int test_elem1 = d_iarr[idx + offset * (4 * i + 1)];
        int test_elem2 = d_iarr[idx + offset * (4 * i + 2)];
        int test_elem3 = d_iarr[idx + offset * (4 * i + 3)];

        if(test_elem0 > first_elem)
        {
            first_elem = test_elem0;
        }
        if(test_elem1 > second_elem)
        {
            second_elem = test_elem1;
        }
        if(test_elem2 > third_elem)
        {
            third_elem = test_elem2;
        }
        if(test_elem3 > fourth_elem)
        {
            fourth_elem = test_elem3;
        }
    }
    if(second_elem > first_elem)
    {
        first_elem = second_elem;
    }
    if(fourth_elem > third_elem)
    {
        third_elem = fourth_elem;
    }
    if(third_elem > first_elem)
    {
        first_elem = third_elem;
    }
    d_iarr[idx] = first_elem;
}

__global__
void gpuMinStep(int * const d_iarr, const size_t n, const size_t offset)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    size_t copy_idx = idx + offset;

    if(copy_idx >= n)
    {
        return;
    }

    int a = d_iarr[idx];
    int b = d_iarr[copy_idx];

    if(b < a)
    {
        a = b;
    }
    d_iarr[idx] = a;
}

void gpuMax(int * const d_iarr, size_t n)
{
    const size_t GPU_STEP_SIZE = 16;
    size_t blocks, threads;
    assert(n % GPU_STEP_SIZE == 0);

    while((n % GPU_STEP_SIZE) == 0)
    {
        size_t total_threads = n / GPU_STEP_SIZE;
        if(total_threads >= 1024)
        {
            blocks = total_threads / 1024;
            threads = 1024;
        }
        else
        {
            blocks = 1;
            threads = total_threads;
        }
        gpuMaxStep16 <<<blocks, threads >>>(d_iarr, n, total_threads);
        n = n / GPU_STEP_SIZE;
    }
    if(n != 0)
    {
        gpuMaxStep <<<1, 1>>> (d_iarr, n, 1, n);
    }
}

void gpuMin(int * const d_iarr, size_t n)
{
    size_t offset = n >> 1;
    size_t blocks, threads;

    while(n > 1)
    {
        if(offset >= 1024)
        {
            blocks = offset / 1024;
            threads = 1024;
        }
        else
        {
            blocks = 1;
            threads = offset;
        }
        gpuMinStep <<<blocks, threads >>>(d_iarr, n, offset);
        n = n >> 1;
        offset = offset >> 1;
    }
}

int gpuGetResult(const int * const d_iarr)
{
    int result;
    checkCudaOK(cudaMemcpy(&result, d_iarr, sizeof(int), cudaMemcpyDeviceToHost));
    return result;
}

void run_test_case_instance(const int * const iarr,
    const size_t n,
    float& cpu_max,
    float& cpu_min,
    float& gpu_max,
    float& gpu_min,
    int& cpu_max_result,
    int& cpu_min_result,
    int& gpu_max_result,
    int& gpu_min_result)
{
    int *d_iarr;
    checkCudaOK(cudaMalloc(&d_iarr, sizeof(int) * n));

    CudaTimer cpu_maxt, cpu_mint, gpu_maxt, gpu_mint;
    cpu_maxt.begin();
    // STL implementation of "Max" that uses a
    // O(n) scan of the array
    cpu_max_result = *std::max_element(iarr, iarr + n);
    cpu_maxt.end();
    cpu_max = cpu_maxt.get();
    cpu_mint.begin();
    // STL implementation of "Min" that uses a
    // O(n) scan of the array
    cpu_min_result = *std::min_element(iarr, iarr + n);
    cpu_mint.end();
    cpu_min = cpu_mint.get();

    checkCudaOK(cudaMemcpy(d_iarr, iarr, sizeof(int) * n, cudaMemcpyHostToDevice));
    gpu_maxt.begin();
    gpuMax(d_iarr, n);
    gpu_maxt.end();
    gpu_max_result = gpuGetResult(d_iarr);
    gpu_max = gpu_maxt.get();

    checkCudaOK(cudaMemcpy(d_iarr, iarr, sizeof(int) * n, cudaMemcpyHostToDevice));
    gpu_mint.begin();
    gpuMin(d_iarr, n);
    gpu_mint.end();
    gpu_min_result = gpuGetResult(d_iarr);
    gpu_min = gpu_mint.get();

    if(cpu_max_result != gpu_max_result)
    {
        printf("Error: cpu_max: %d, gpu_max: %d\n", cpu_max_result, gpu_max_result);
    }
    assert(cpu_max_result == gpu_max_result);
    assert(cpu_min_result == gpu_min_result);

    checkCudaOK(cudaFree(d_iarr));
}

void run_test_case(const size_t n, const int * const iarr, int trials)
{
    size_t raw_n = n * (1 << 20);

    float cpu_max = 0.0, cpu_min = 0.0, gpu_max = 0.0, gpu_min = 0.0;
    int cpu_max_result, cpu_min_result, gpu_max_result, gpu_min_result;
    for(int i = 0; i < trials; ++i)
    {
        float icpu_max, icpu_min, igpu_max, igpu_min;
        run_test_case_instance(iarr, 
        raw_n, 
        icpu_max, 
        icpu_min, 
        igpu_max, 
        igpu_min,
        cpu_max_result,
        cpu_min_result,
        gpu_max_result,
        gpu_min_result);

        cpu_max += icpu_max;
        cpu_min += icpu_min;
        gpu_max += igpu_max;
        gpu_min += igpu_min;
    }

    cpu_max /= (1000 * trials);
    cpu_min /= (1000 * trials);
    gpu_max /= (1000 * trials);
    gpu_min /= (1000 * trials);

    printf("N: %lu", n);
    printf("M GPUmax: %d CPUmax: %d GPUtime: %.6f CPUtime: %.6f GPUSpeedup: %.1f ",
        gpu_max_result,
        cpu_max_result,
        gpu_max,
        cpu_max,
        cpu_max / gpu_max);
    printf("N: %lu", n);
    printf("M GPUmin: %d CPUmin: %d GPUtime: %.6f CPUtime: %.6f GPUSpeedup: %.1f\n",
        gpu_min_result,
        cpu_min_result,
        gpu_min,
        cpu_min,
        cpu_min / gpu_min);
}


int main(int argc, char *argv[])
{
#ifdef VERBOSE_DEBUG
    CudaTimer timer;
    timer.begin();
#endif
    printf("FIRSTNAME: SUVANJAN\n");
    printf("LASTNAME: MUKHERJEE\n");

    int max_size = 32;
    int trials = 10;
    int try_size = -1;

    if(argc > 1)
    {
        try_size = std::atoi(argv[1]);

        if(try_size > 0)
        {
            max_size = try_size;
        }
        else
        {
            try_size = -1;
        }
    }

    int *iarr = alloc_and_init(max_size * (1 << 20));

    if(try_size == -1)
    {
        run_test_case(2, iarr, trials);
        run_test_case(8, iarr, trials);
        run_test_case(32, iarr, trials);
    }
    else
    {
        run_test_case(try_size, iarr, trials);
    }

    delete []iarr;

#ifdef VERBOSE_DEBUG
    timer.end();
    printf("Total elapsed time was %.6fs\n", timer.get() / 1000);
#endif
    return 0;
}
