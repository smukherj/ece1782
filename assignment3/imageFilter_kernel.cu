#ifndef _IMAGEFILTER_KERNEL_H_
#define _IMAGEFILTER_KERNEL_H_

__global__ void imageFilterKernelPartA(char3* inputPixels, char3* outputPixels, uint width, uint height)
{

	uint size = width * height;
	int num_threads = blockDim.x * gridDim.x;
	int chunkSize = (size / num_threads) + 1;

	int baseIdx = (blockIdx.x * blockDim.x + threadIdx.x) * chunkSize;

	for(int idx = 0; idx < chunkSize; ++idx)
	{

		int curIdx = baseIdx + idx;
		if(curIdx >= size)
		{
			return;
		}

		int3 sum = make_int3(0, 0, 0);
		int count = 0;
		int y = curIdx / width;
		int x = curIdx - y * width;

		for(int i = -4; i < 4; ++i)
		{
			int ay = i + y;
			for(int j = -4; j < 4; ++j)
			{
				int ax = j + x;

				if(ax < 0 || ay < 0 || ay >= height || ax >= width)
				{
					continue;
				}


				sum.x += (int) inputPixels[curIdx + i * width + j].x;
				sum.y += (int) inputPixels[curIdx + i * width + j].y;
				sum.z += (int) inputPixels[curIdx + i * width + j].z;
				count++;

#if 0
				if(x == 1277 && y == 0)
				{
					printf("(%d, %d) idx %d (%x, %x, %x)\n", ax, ay, idx, inputPixels[curIdx + i * width + j].x & 0xff, 
						inputPixels[curIdx + i * width + j].y & 0xff, 
						inputPixels[curIdx + i * width + j].z & 0xff);
				}
#endif
			}
		}

		outputPixels[curIdx].x = sum.x / count;
		outputPixels[curIdx].y = sum.y / count;
		outputPixels[curIdx].z = sum.z / count;

#if 0
		if(x == 1277 && y == 0)
		{
			printf("(%d, %d) (%d, %d, %d) %d\n", x, y, sum.x, sum.y, sum.z, count);
		}
#endif
	}

}
__global__ void imageFilterKernelPartB(char3* inputPixels, char3* outputPixels, uint width, uint height)
{

	uint size = width * height;
	int num_threads = blockDim.x * gridDim.x;

	for(int curIdx = blockIdx.x * blockDim.x + threadIdx.x; curIdx < size; curIdx += num_threads)
	{

		if(curIdx >= size)
		{
			return;
		}

		int3 sum = make_int3(0, 0, 0);
		int count = 0;
		int y = curIdx / width;
		int x = curIdx - y * width;

		for(int i = -4; i < 4; ++i)
		{
			int ay = i + y;
			for(int j = -4; j < 4; ++j)
			{
				int ax = j + x;

				if(ax < 0 || ay < 0 || ay >= height || ax >= width)
				{
					continue;
				}

				sum.x += (int) inputPixels[curIdx + i * width + j].x;
				sum.y += (int) inputPixels[curIdx + i * width + j].y;
				sum.z += (int) inputPixels[curIdx + i * width + j].z;
				count++;
			}
		}

		outputPixels[curIdx].x = sum.x / count;
		outputPixels[curIdx].y = sum.y / count;
		outputPixels[curIdx].z = sum.z / count;
	}

}

#define BLOCK_SIZE 128
#define WBLOCK_SIZE 120
#define W2BLOCK_SIZE 124

// For debugging
#define TEST_KERNEL 0
#define KTEST_X 0
#define KTEST_Y 851

__device__ void loadBlock(char3 *inputPixels,  char3 *s_inputPixels, int ablock_x, int ablock_y, uint width, uint height
#if TEST_KERNEL
, int iblock
#endif
)
{
	for(int i = ablock_y; i < (ablock_y + BLOCK_SIZE); i += 32)
	{
		for(int j = ablock_x; j < (ablock_x + BLOCK_SIZE); j += 32)
		{
			int y = i + threadIdx.y;
			int x = j + threadIdx.x;

			int sx = x - ablock_x;
			int sy = y - ablock_y;

			if(x < (ablock_x + BLOCK_SIZE) && y < (ablock_y + BLOCK_SIZE))
			{
				s_inputPixels[sy * BLOCK_SIZE + sx].x = inputPixels[y * width + x].x;
				s_inputPixels[sy * BLOCK_SIZE + sx].y = inputPixels[y * width + x].y;
				s_inputPixels[sy * BLOCK_SIZE + sx].z = inputPixels[y * width + x].z;

			}
		}
	}
}

__device__ void blurBlock(char3 *outputPixels,  char3 *s_inputPixels, int ablock_x, int ablock_y, uint width, uint height
#if TEST_KERNEL
, int iblock
#endif
)
{
	for(int i = ablock_y; i < (ablock_y + W2BLOCK_SIZE); i += 32)
	{
		int sy = i - ablock_y + threadIdx.y;
		int ay = i + threadIdx.y;

		if(ablock_y > 0)
		{
			ay += 4;
			sy += 4;
		}

		if(ay >= height || ay >= (ablock_y + W2BLOCK_SIZE))
		{
			continue;
		}
		for(int j = ablock_x; j < (ablock_x + W2BLOCK_SIZE); j += 32)
		{
			int sx = j - ablock_x + threadIdx.x;
			int ax = j + threadIdx.x;

			if(ablock_x > 0)
			{
				sx += 4;
				ax += 4;
			}

			if(ax >= width)
			{
				continue;
			}

			if(ax >= (ablock_x + W2BLOCK_SIZE))
			{
				continue;
			}

			int3 sum = make_int3(0, 0, 0);
			int count = 0;
			int iimax = sy + 4;
			int ijmax = sx + 4;


			if(ay + 4 >= height)
			{
				iimax = sy + height - ay;
			}
			if(iimax > 127)
			{
				iimax = 127;
			}
			if(ax + 4 >= width)
			{
				ijmax = sx + width - ax;
			}

			if(ijmax > 127)
			{
				ijmax = 127;
			}



			for(int ii = sy > 3 ? sy - 4 : 0; ii < iimax; ++ii)
			{
				for(int ij = sx > 3 ? sx - 4 : 0; ij < ijmax; ++ij)
				{
					sum.x += (int) s_inputPixels[ii* BLOCK_SIZE + ij].x;
					sum.y += (int) s_inputPixels[ii* BLOCK_SIZE + ij].y;
					sum.z += (int) s_inputPixels[ii* BLOCK_SIZE + ij].z;
					count++;
				}
			}

			outputPixels[ay * width + ax].x = sum.x / count;
			outputPixels[ay * width + ax].y = sum.y / count;
			outputPixels[ay * width + ax].z = sum.z / count;

#if TEST_KERNEL
			if(ax == KTEST_X && ay == KTEST_Y)
			{
				printf("(%d, %d) -> (%d, %d) (%d, %d, %d) Block (%d, %d), BlockIdx %d, iblock %d, thread (%d, %d)\n", 
					sx, sy, ax, ay,
					outputPixels[ay * width + ax].x & 0xff,
					outputPixels[ay * width + ax].y & 0xff,
					outputPixels[ay * width + ax].z & 0xff,
					ablock_x, ablock_y, blockIdx.x, iblock, threadIdx.x, threadIdx.y);
			}
#endif

#if TEST_KERNEL
			if(ax == KTEST_X && ay == KTEST_Y)
			{
				printf("(%d, %d) (%d, %d, %d) %d, block %d, thread (%d, %d), blockxy (%d, %d)\n", 
					ax, ay, sum.x, sum.y, sum.z, count,
					blockIdx.x, threadIdx.x, threadIdx.y, ablock_x, ablock_y);
			}
#endif
		}
	}
}

__global__ void imageFilterKernelPartC(char3* inputPixels, char3* outputPixels, uint width, uint height)
{
	int num_x_steps = width / WBLOCK_SIZE + ((width % WBLOCK_SIZE == 0) ? 0 : 1);
	int num_y_steps = height / WBLOCK_SIZE + ((height % WBLOCK_SIZE == 0) ? 0 : 1);

	int blockSteps = num_x_steps * num_y_steps;

	__shared__  char3 s_inputPixels[BLOCK_SIZE * BLOCK_SIZE];

#if TEST_KERNEL
	if(blockIdx.x == 0 && threadIdx.x == 0 && threadIdx.y == 0)
	{
		printf("x_steps %d, y_steps %d, steps %d\n", num_x_steps, num_y_steps, blockSteps);
	}
#endif

	// The kernel has 12 thread blocks. Each thread block will
	// be working on a 128 x 128 array of pixels at a time.
	// Load the 128 x 128 into shared memory, then blur
	// 32 x 32 segments
	for(int iblock = blockIdx.x; iblock < blockSteps; iblock += 12)
	{
		int block_y = iblock / num_x_steps;
		int block_x = iblock - block_y * num_x_steps;

		int ablock_x = block_x * WBLOCK_SIZE;
		int ablock_y = block_y * WBLOCK_SIZE;

		__syncthreads();

		loadBlock(inputPixels, s_inputPixels, ablock_x, ablock_y, width, height
#if TEST_KERNEL
		, iblock
#endif
		);

		__syncthreads();

		
		blurBlock(outputPixels, s_inputPixels, ablock_x, ablock_y, width, height
#if TEST_KERNEL
		, iblock
#endif
		);

	}
}

#endif // _IMAGEFILTER_KERNEL_H_
