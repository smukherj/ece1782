import os
import subprocess
import glob
import hashlib

total_cases = 0
next_report = 10

def run_conversion(in_file, part, fp):
	out_file = os.path.join('work', in_file + '_part%s'%part + '.bmp')
	p = subprocess.Popen(['./imageFilter', in_file, out_file, part], stdout=fp, stderr=fp)
	ret = p.wait()

	return ret, out_file

def check_conversion(file_list):
	result = True
	cksum_list = []
	for ifile in file_list:
		for jfile in file_list:
			if ifile == jfile:
				continue
			icksum = hashlib.md5(open(ifile, 'rb').read()).hexdigest()
			jcksum = hashlib.md5(open(jfile, 'rb').read()).hexdigest()

			if icksum != jcksum:
				print 'ERROR: %s and %s differ'%(ifile, jfile)
				result = False
	return result

def report_progress(total):
	global next_report
	progress = (float(total) / float(total_cases)) * 100;
	if progress >= next_report:
		print 'Progress %.0f%%'%progress
		next_report += 10
	return


f_list = glob.glob('*.bmp')
total_cases = len(f_list) * 3
fp = open('work/out.txt', 'w')
errors = 0
total = 0
for ifile in f_list:
	outfiles = []
	for part in ('a', 'b', 'c'):
		total += 1
		ret, out_file = run_conversion(ifile, part, fp)
		outfiles.append(out_file)
		if  ret != 0:
			errors += 1
			print 'ERROR: File %s Part %s conversion FAILED'%(ifile, part)
		report_progress(total)
	if not check_conversion(outfiles):
		print 'ERROR: File %s FAILED in diff check'%(ifile)

if errors > 0:
	print 'ERROR: %d conversions FAILED.'
rate = 0.0
if total > 0:
	print '%.2f%% cases passed'%((1 - errors / total) * 100)

if errors:
	exit(-1)
exit(0)