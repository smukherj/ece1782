#include <stdio.h>
#include <cuda_runtime.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>
#include <algorithm>

#define checkCudaOK(val) {\
    if(val != cudaSuccess) {\
        fprintf(stderr, "Cuda check failed at %s:%d '%s'\n", __FILE__, __LINE__, #val); \
        fprintf(stderr, "%s\n", cudaGetErrorString(val));\
        exit(EXIT_FAILURE);\
    }\
}

#define CHUNK_SIZE 4
#define XCHUNK_SIZE 2
#define EPSILON 0.1

class CudaTimer
{
public:
    CudaTimer()
    {
        cudaEventCreate(&m_begin);
        cudaEventCreate(&m_end);
    }

    ~CudaTimer()
    {
        cudaEventDestroy(m_begin);
        cudaEventDestroy(m_end);
    }

    void begin()
    {
        cudaEventRecord(m_begin, 0);
    }

    void end()
    {
        cudaEventRecord(m_end, 0);
    }

    float get()
    {
        float duration = 0.0f;
        cudaEventSynchronize(m_begin);
        cudaEventSynchronize(m_end);
        cudaEventElapsedTime(&duration, m_begin, m_end);
        return duration;
    }

private:
    CudaTimer(CudaTimer& rhs)
    {
        // Disallow copying
    }

    cudaEvent_t m_begin;
    cudaEvent_t m_end;
};

bool cudaNoErrors()
{
    cudaError_t errSync  = cudaGetLastError();
    cudaError_t errAsync = cudaDeviceSynchronize();
    return (errSync == cudaSuccess) && (errAsync == cudaSuccess);
}

__global__
void arradd(float * const d_A, size_t aSize, float x)
{
	int idx = (blockIdx.x * blockDim.x + threadIdx.x) * CHUNK_SIZE;

	int n = idx + CHUNK_SIZE;
	if(n >= aSize)
	{
		n = aSize;
	}

	for(int i = idx; i < n; ++i)
	{
		d_A[i] += x;
	}
}

__global__
void darradd(double * const d_A, size_t aSize, double x)
{
	int idx = (blockIdx.x * blockDim.x + threadIdx.x) * CHUNK_SIZE;

	int n = idx + CHUNK_SIZE;
	if(n >= aSize)
	{
		n = aSize;
	}

	for(int i = idx; i < n; ++i)
	{
		d_A[i] += x;
	}
}

__global__
void iarradd(int * const d_A, size_t aSize, int x)
{
	int idx = (blockIdx.x * blockDim.x + threadIdx.x) * CHUNK_SIZE;

	int n = idx + CHUNK_SIZE;
	if(n >= aSize)
	{
		n = aSize;
	}

	for(int i = idx; i < n; ++i)
	{
		d_A[i] += x;
	}
}

__global__
void xarradd(float * const d_A, size_t aSize, float x, int nx)
{
	int idx = (blockIdx.x * blockDim.x + threadIdx.x) * XCHUNK_SIZE;

	int n = idx + XCHUNK_SIZE;
	if(n >= aSize)
	{
		n = aSize;
	}

	for(int i = idx; i < n; ++i)
	{
		for(int j = 0; j < nx; ++j)
		{
			d_A[i] += x;
		}
	}
}

void init_arrA(float * const h_A, int n)
{
	for(int i = 0; i < n; ++i)
	{
		h_A[i] = i / 3.0f;
	}
}

void run_parta()
{
	printf("Running Part A...\n");

	size_t bytes = 256e6 * sizeof(float);
	float *h_A = (float*)malloc(bytes);
	int ielems = 1e6;

	printf("Elements(M)\tCPUtoGPU(ms)\tKernel(ms)\tGPUtoCPU(ms)\n");
	while(ielems <= 256e6)
	{
		int num_blocks = (ielems / (CHUNK_SIZE * 1024)) + 1;
		//printf("Elements: %d, Blocks: %d\n", ielems, num_blocks);
		init_arrA(h_A, ielems);
		float *d_A = NULL;
		checkCudaOK(cudaMalloc(&d_A, sizeof(float) * ielems));
		CudaTimer h2d, k, d2h;
		h2d.begin();
		checkCudaOK(cudaMemcpy(d_A, h_A, sizeof(float) * ielems, cudaMemcpyHostToDevice));
		h2d.end();
		k.begin();
		arradd<<<num_blocks, 1024>>> (d_A, ielems, 3.63);
		k.end();
		d2h.begin();
		checkCudaOK(cudaMemcpy(h_A, d_A, sizeof(float) * ielems, cudaMemcpyDeviceToHost));
		d2h.end();
		checkCudaOK(cudaFree(d_A));

		if(cudaNoErrors())
		{
			printf("%d\t%.2f\t%.2f\t%.2f\n", static_cast<int>(ielems / 1e6), h2d.get(), k.get(), d2h.get());
			ielems *= 2;
		}
		else
		{
			break;
		}
	}

	free(h_A);
	printf("Part A completed.\n");
}

void init_arrB(double * const h_A, int n)
{
	for(int i = 0; i < n; ++i)
	{
		h_A[i] = i / 3.0;
	}
}

void check_B(double * const h_A, int n)
{
	for(int i = 0; i < n; ++i)
	{
		double val = i / 3.0;
		val += 3.63;

		double diff = std::max(val, h_A[i]) - std::min(val, h_A[i]);
		if(diff > EPSILON)
		{
			fprintf(stderr, "Check failed at index %d. Difference greater than %.4f. Val %.4f, Expected %.4f\n", 
			i, 
			EPSILON, 
			h_A[i], 
			val);
			exit(EXIT_FAILURE);
		}
	}
}

void run_partb()
{
	printf("Running Part B...\n");
	size_t bytes = 256e6 * sizeof(double);
	double *h_A = (double*)malloc(bytes);
	int ielems = 1e6;

	printf("Elements(M)\tCPUtoGPU(ms)\tKernel(ms)\tGPUtoCPU(ms)\n");
	while(ielems <= 256e6)
	{
		int num_blocks = (ielems / (CHUNK_SIZE * 1024)) + 1;
		//printf("Elements: %d, Blocks: %d\n", ielems, num_blocks);
		init_arrB(h_A, ielems);
		double *d_A = NULL;
		checkCudaOK(cudaMalloc(&d_A, sizeof(double) * ielems));
		CudaTimer h2d, k, d2h;
		h2d.begin();
		checkCudaOK(cudaMemcpy(d_A, h_A, sizeof(double) * ielems, cudaMemcpyHostToDevice));
		h2d.end();
		k.begin();
		darradd<<<num_blocks, 1024>>> (d_A, ielems, 3.63);
		k.end();
		d2h.begin();
		checkCudaOK(cudaMemcpy(h_A, d_A, sizeof(double) * ielems, cudaMemcpyDeviceToHost));
		d2h.end();
		checkCudaOK(cudaFree(d_A));

		if(cudaNoErrors())
		{
			check_B(h_A, ielems);
			printf("%d\t%.2f\t%.2f\t%.2f\n", static_cast<int>(ielems / 1e6), h2d.get(), k.get(), d2h.get());
			ielems *= 2;
		}
		else
		{
			break;
		}
	}

	free(h_A);
	printf("Part B completed.\n");
}

void init_arrC(int * const h_A, int n)
{
	for(int i = 0; i < n; ++i)
	{
		h_A[i] = i / 3.0f;
	}
}

void check_C(int * const h_A, int n)
{
	for(int i = 0; i < n; ++i)
	{
		int val = i / 3.0f;
		val += 3;

		if(val != h_A[i])
		{
			fprintf(stderr, "Check failed at index %d. Val %d, Expected %d\n", 
			i, 
			h_A[i], 
			val);
			exit(EXIT_FAILURE);
		}
	}
}

void run_partc()
{
	printf("Running Part C...\n");
	size_t bytes = 256e6 * sizeof(int);
	int *h_A = (int*)malloc(bytes);
	int ielems = 1e6;

	printf("Elements(M)\tCPUtoGPU(ms)\tKernel(ms)\tGPUtoCPU(ms)\n");
	while(ielems <= 256e6)
	{
		int num_blocks = (ielems / (CHUNK_SIZE * 1024)) + 1;
		//printf("Elements: %d, Blocks: %d\n", ielems, num_blocks);
		init_arrC(h_A, ielems);
		int *d_A = NULL;
		checkCudaOK(cudaMalloc(&d_A, sizeof(int) * ielems));
		CudaTimer h2d, k, d2h;
		h2d.begin();
		checkCudaOK(cudaMemcpy(d_A, h_A, sizeof(int) * ielems, cudaMemcpyHostToDevice));
		h2d.end();
		k.begin();
		iarradd<<<num_blocks, 1024>>> (d_A, ielems, 3);
		k.end();
		d2h.begin();
		checkCudaOK(cudaMemcpy(h_A, d_A, sizeof(int) * ielems, cudaMemcpyDeviceToHost));
		d2h.end();
		checkCudaOK(cudaFree(d_A));

		if(cudaNoErrors())
		{
			check_C(h_A, ielems);
			printf("%d\t%.2f\t%.2f\t%.2f\n", static_cast<int>(ielems / 1e6), h2d.get(), k.get(), d2h.get());
			ielems *= 2;
		}
		else
		{
			break;
		}
	}

	free(h_A);
	printf("Part C completed.\n");
}

void run_partd()
{
	printf("Running Part D...\n");
	size_t bytes = 256e6 * sizeof(float);
	float *h_A = (float*)malloc(bytes);
	const int ielems = 128e6;
	const int num_blocks = (ielems / (XCHUNK_SIZE * 1024)) + 1;
	int nx = 1;

	printf("XaddedTimes\tElements(M)\tCPUtoGPU(ms)\tKernel(ms)\tGPUtoCPU(ms)\n");
	while(nx <= 256)
	{
		//printf("Elements: %d, Blocks: %d\n", ielems, num_blocks);
		init_arrA(h_A, ielems);
		float *d_A = NULL;
		checkCudaOK(cudaMalloc(&d_A, sizeof(float) * ielems));
		CudaTimer h2d, k, d2h;
		h2d.begin();
		checkCudaOK(cudaMemcpy(d_A, h_A, sizeof(float) * ielems, cudaMemcpyHostToDevice));
		h2d.end();
		k.begin();
		xarradd<<<num_blocks, 1024>>> (d_A, ielems, 3.63, nx);
		k.end();
		d2h.begin();
		checkCudaOK(cudaMemcpy(h_A, d_A, sizeof(float) * ielems, cudaMemcpyDeviceToHost));
		d2h.end();
		checkCudaOK(cudaFree(d_A));

		if(cudaNoErrors())
		{
			printf("%d\t%d\t%.2f\t%.2f\t%.2f\n", nx, static_cast<int>(ielems / 1e6), h2d.get(), k.get(), d2h.get());
			nx *= 2;
		}
		else
		{
			break;
		}
	}
	printf("Part D completed.\n");
}


int main()
{
	run_parta();
	run_partb();
	run_partc();
	run_partd();
	return 0;
}