Elements(M)  CPUtoGPU(ms)  Kernel(ms)    GPUtoCPU(ms)
1            0.45          0.07          0.49
2            1.01          0.13          0.98
4            2.58          0.25          2.45
8            4.80          0.49          4.92
16           9.26          0.98          9.51
32           18.01         1.94          19.64
64           35.70         3.87          39.23
128          70.71         7.73          78.45
256          141.09        15.44         156.70